# load library
import math
import numpy as np
import time as tm

# give the entries in matrix A and column vector f
A = np.array([[-3, 1, 2],[-1, -3, 1],[-1, 3, 4]])  

f = np.array([2, 6, 4])

# decompose A to D and R matrices
D = np.zeros([3,3])
Dinv = np.zeros([3,3])

for i in range(3):
    D[i,i]=A[i,i]
    
# D and R matrices from A
R = A-D
## Test add a line to the file in branch.(I cannot add a line in GitLab branch.)

# inverse of D matrix
for i in range(3):
    Dinv[i,i]=1/D[i,i]
    

# initial guess value of x
x = np.array([0, 0, 0])

# walltime before matrix solving
cpu_tm_bg = tm.time()

# iteration
for iter in range(200):
    # use function np.matmul for matrix-vector multiplication
    x=np.matmul(Dinv,f-np.matmul(R,x))

    # residual during iterations
    residual=f-np.matmul(A,x)
    
    # use calculate the norm of the residual using numpy function
    
    norm=math.sqrt(residual[0]**2+residual[1]**2+residual[2]**2)
    
# walltime after matrix solving
cpu_tm_ed = tm.time()    

# print results
print(cpu_tm_ed - cpu_tm_bg, norm)

